// console.log("Hello World");

// MAP [#3-4]
fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})
.then(response => response.json())

.then(data => {
	let array = data.map(element => element.title)
	console.log(array);
})



// GET [#5-6]
fetch("https://jsonplaceholder.typicode.com/todos/2", {method: "GET"})
.then(response => response.json())
.then(result => console.log(`The item "${result.title}" on the list has a status of ${result.completed}.`));



// POST [#7]
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created to do list",
		userId: 1

	})
})
.then(response => response.json())
.then(result => console.log(result));



// PUT [#8]
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Update my to do list",
		userId: 1
	})
})
.then(response => response.json())
.then(result => console.log(result));




// Updating the structure of the document [#9]
fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({

			completed: false,
			title: "Update my to do list",
			description: "To update the my to do list with a diff data structure",
			status: "Pending",
			dateCompleted: "Pending",
			userId: 1
			
		})
	})
.then(response => response.json())
.then(result => console.log(result));




// Updating Status to Completed [#10-11]
fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({

			completed: true,
			title: "Update my to do list",
			description: "Completed my to do list item",
			status: "Completed",
			dateCompleted: "01/31/23",
			userId: 1
			
		})
	})
.then(response => response.json())
.then(result => console.log(result));




// Deleting an item [#12]
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "DELETE"})
.then(response => response.json())
.then(result => console.log(result));